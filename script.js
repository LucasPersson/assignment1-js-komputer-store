// initiate variables that has to be used in order for functionallty to work
let bankBalance = 0
let payBalance = 0
let totalLoanBalanace = 0
let debt = false
let computerPurchaseBeforeLoan = false
let computerArr = []
let tempComputerPrice 
let tempComputerTitle


// bank functions for the bank section in webpage

//loan button functions

const loanButton = document.getElementById('loanBtn')


loanButton.addEventListener('click', function () {

  let loanPrompt =  prompt('How much do you want to loan? You cant loan more than doubble you bank balance.')
  let loanAmount = parseInt(loanPrompt)

    // checks to apply correct loan requirements
    if (debt === true) {

        alert(`Pay your loan!!`)
        return

    } else if(computerPurchaseBeforeLoan === true) {


      alert(`Purchase a computer before taking a new loan!!`)
        return


    } else if (loanAmount <= bankBalance * 2) {

        totalLoanBalanace += loanAmount
        bankBalance += totalLoanBalanace
        document.getElementById('contentLoanBalance').innerHTML=`Outstanding Loan ${totalLoanBalanace} Kr.`
        document.getElementById('totalBankBalance').innerHTML=`${bankBalance} Kr.`
        debt = true
        computerPurchaseBeforeLoan = true
        document.getElementById('payLoanBtn').style.display = 'block'

    } else {

        return alert(`Choose another amount.`)
    }
})


// Work functions


// work section bank button fucntions

const bankButton = document.getElementById('bankBtn')


bankButton.addEventListener('click', function () {

  // checks requirmens on the bank button 
  if (payBalance !== 0) {
    
      if (debt === true ) {

        let removeTenLoan = payBalance
        bankBalance += 0.9 * payBalance
       

        if (totalLoanBalanace > 0) {
          
          Math.abs(totalLoanBalanace -= removeTenLoan * 0.1)
          if (totalLoanBalanace <= 0 ) {
            bankBalance +=  Math.abs(totalLoanBalanace)
            debt = false
            totalLoanBalanace = 0
          }
        
            document.getElementById('totalBankBalance').innerHTML=`${bankBalance} Kr.`
            document.getElementById('contentLoanBalance').innerHTML=`Outstanding Loan ${totalLoanBalanace} Kr.`  
        }


      } else {

        bankBalance += payBalance
      }
  }


  payBalance = 0
  document.getElementById('totalWorkBalance').innerHTML=`${payBalance} Kr.`
  document.getElementById('totalBankBalance').innerHTML=`${bankBalance} Kr.`
})




//work button functions

const workButton = document.getElementById('workBtn')


workButton.addEventListener('click', function () {

  payBalance += 100
  document.getElementById('totalWorkBalance').innerHTML=`${payBalance} Kr.`
})


// work section pay loan button fucntions

//show or hide based on loan
if (debt === false) {

  document.getElementById('payLoanBtn').style.display = 'none'
} 

const payLoanButton = document.getElementById('payLoanBtn')


payLoanButton.addEventListener('click', function () {


  //checks so it can aply the correct values
  if (totalLoanBalanace - payBalance <= 0) {

    bankBalance += Math.abs(totalLoanBalanace - payBalance)
    document.getElementById('totalBankBalance').innerHTML=`${bankBalance} Kr.`
    totalLoanBalanace = 0
    debt = false
  
  } else {
    totalLoanBalanace -= payBalance
  }

  payBalance = 0
  document.getElementById('totalWorkBalance').innerHTML=`${payBalance} Kr.`
  document.getElementById('contentLoanBalance').innerHTML=`Outstanding Loan ${totalLoanBalanace} Kr.`
})




//  api data

//get the htmlelements to fill with data from api later
const computerSelect = document.getElementById('computerSelect')
const computerFeat = document.getElementById('computerFeat')
const computerImg = document.getElementById('computerImg')
const computerName = document.getElementById('computerName')
const computerInfo = document.getElementById('computerInfo')
const computerPrice = document.getElementById('computerPrice')

//getting data from api
async function getComputerData(){
  try {
      await fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
      .then(response => response.json())
      .then(data => computerArr = data)
      .then(computerArr => populateLaptopSelect(computerArr))
  } catch (e) {
      console.error(e.name + ': ' + e.message)
  }
}
getComputerData()


// iterating over the api data 
const populateLaptopSelect = (computerArr) => {
  computerArr.forEach(x => addComputer(x))
  setComputerContent(computerArr[0])
}

// poppulating select button with computer names
const addComputer = (computerArr) => {
  const computerOption = document.createElement('option')
  computerOption.value = computerArr.id
  computerOption.appendChild(document.createTextNode(computerArr.title))
  computerSelect.appendChild(computerOption)
}


// filling all the html content with the data from api
const setComputerContent = (computerArr) => {

 
  computerName.innerText=computerArr.title
  computerPrice.innerText = computerArr.price
  computerFeat.innerText = ''
  setFeatSpecInList(computerArr)
  if (computerArr.title === "The Visor" ) {
    computerImg.src = `https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png`
  } else {
    computerImg.src = `https://noroff-komputer-store-api.herokuapp.com/${computerArr.image}`
  }
  computerName.innerText = computerArr.title
  computerInfo.innerText = computerArr.description
  computerPrice.innerText = computerArr.price + ` Kr.`

  tempComputerPrice = computerArr.price
  tempComputerTitle = computerArr.title
}


// adds specs to featuers in html
const setFeatSpecInList = (computerArr) => {
  
  for (const specs of computerArr.specs) {
    
  const computerSpecs = document.createElement('li')

  computerSpecs.value = specs
  computerSpecs.appendChild(document.createTextNode(specs))
  computerFeat.appendChild(computerSpecs)
  computerSpecs.id = 'specsList'
 }
}

//handle the index of the data so it displays correct data for selected computer
const changeComputerContent = e => {
  const selectedComputer = computerArr[e.target.selectedIndex]
  setComputerContent(selectedComputer)
}

computerSelect.addEventListener('change', changeComputerContent)


//BUY NOW functionallity

const purchaseButton = document.getElementById('computerBtn')


purchaseButton.addEventListener('click', function () {

  // check for the correct ammount in bank

    if (bankBalance >= tempComputerPrice) {

      alert(`You are now the owner of the new laptop ${tempComputerTitle}!`)
      bankBalance -= tempComputerPrice
      document.getElementById('totalBankBalance').innerHTML=`${bankBalance} Kr.`
      computerPurchaseBeforeLoan = false

    } else {
      let requierdMoney = tempComputerPrice - bankBalance
      alert(`You are missing ${requierdMoney}. Get more money! `)
    }
})